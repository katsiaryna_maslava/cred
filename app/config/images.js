/* @flow */

export default {
  PORTFOLIO_TAB_ICON: require('../../assets/images/portfolio_tab.png'),
  PRICE_TAB_ICON: require('../../assets/images/price_tab.png'),
  NEWS_TAB_ICON: require('../../assets/images/news_tab.png'),
  CRYPTO_TAB_ICON: require('../../assets/images/crypto_tab.png'),

  GRADIENT_1: require('../../assets/images/gradient1.png'),
  GRADIENT_2: require('../../assets/images/gradient2.png'),
  GRADIENT_3: require('../../assets/images/gradient3.png'),
  GRADIENT_4: require('../../assets/images/gradient4.png')
};
