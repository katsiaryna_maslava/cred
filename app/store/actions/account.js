import {accountAction} from './types/accountTypes';
import {signup, updateUser, getConfirmationCode, checkConfirmationCode} from '../api';


export const createAccount = (user) => async (dispatch) => {
	try {
		const {data} = await signup(user);
		dispatch({type: accountAction.SET_DATA, payload: {data: {...data.users[0], token: data.token}}});
	} catch (e) {
		console.log(e.response);
		dispatch({type: accountAction.SET_ERRORS, payload: {errors: {emailAndPass: 'Invalid email or password.'}}});
	}
};

export const updateUserInformation = (user, updateInfo) => async (dispatch) => {
	try {
		const {data} = await updateUser(user.id, updateInfo, user.token);
		user.token = data.token;
		Object.assign(user, updateInfo);
		dispatch({type: accountAction.SET_DATA, payload: {data: {}}});
		dispatch({type: accountAction.SET_DATA, payload: {data: user}});
	} catch (e) {
		console.log(e.response);
		dispatch({type: accountAction.SET_ERRORS, payload: {errors: {userInfo: e.response.data ? e.response.data.errorMessage : 'Invalid user info.'}}});
	}
};

export const getCode = (user) => async (dispatch) => {
	try {
		const {data} = await getConfirmationCode(user.id, user.phone, user.token);
		user.token = data.token;
		dispatch({type: accountAction.SET_DATA, payload: {data: user}});
	} catch (e) {
		console.log(e.response);
		dispatch({type: accountAction.SET_ERRORS, payload: {errors: {phone: e.response.data ? e.response.data.errorMessage : 'Invalid phone.'}}});
	}
};

export const checkCode = (user) => async (dispatch) => {
	try {
		const {data} = await checkConfirmationCode(user.id, user.code, user.token);
		user.token = data.token;
		dispatch({type: accountAction.SET_DATA, payload: {data: user}});
	} catch (e) {
		console.log(e.response);
		dispatch({type: accountAction.SET_ERRORS, payload: {errors: {code: e.response.data ? e.response.data.errorMessage : 'Invalid code.'}}});
	}
};

export const login = (user) => async (dispatch) => {
	try {
		const {data} = await logins(user);
		console.log(data);
		dispatch({type: accountAction.SET_DATA, payload: {data}});
	} catch (e) {
		console.log(e.response);
		dispatch({type: accountAction.SET_ERRORS, payload: {errors: {code: e.response.data ? e.response.data.errorMessage : 'Invalid email or password.'}}});
	}
};