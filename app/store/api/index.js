import axios from 'axios';

const baseUrl = 'https://iwsh5wr097.execute-api.us-west-2.amazonaws.com/development';
const keyApi = 'YWtL6CYb3q8CqyrS8RQdp8wgBpSvu9nj3vAqMe56';

// export const api = axios.create({
// 	baseUrl,
// 	timeout: 25000,
// 	headers: {
// 		'x-api-key': keyApi,
// 		'Content-Type': 'application/json'
// 	}
// });

let api;
const getInitializedApi = () => {
	if (api) return api;
	return (api = axios.create({
		baseURL: baseUrl,
		headers: {
			'x-api-key': keyApi,
			'Content-Type': 'application/json'
		},
		responseType: 'json'
	}));
};

export const signup = async (user) => {
	return await getInitializedApi().post('/users/create?getToken=true', {users: [user]});
};

export const updateUser = async (userId, updateFields, token) => {
	getInitializedApi().defaults.headers.token = token;
	return await getInitializedApi().put(`/users/${userId}`, updateFields)
};

export const getConfirmationCode = async (userId, phone, token) => {
	getInitializedApi().defaults.headers.token = token;
	return await getInitializedApi().post(`/users/${userId}/codes`, {phone})
};

export const checkConfirmationCode = async (userId, code, token) => {
	getInitializedApi().defaults.headers.token = token;
	return await getInitializedApi().put(`/users/${userId}/codes`, {code})
};

export const logins = async (user) => {
	return await getInitializedApi().post(`/logins`, user)
};
export const sendAddress = async (user, address) => {
    getInitializedApi().defaults.headers.token = token;
    return await getInitializedApi().post(`/users/${user.userId}`, {address});
};