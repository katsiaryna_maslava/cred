import {AccountAction} from './actions';

const initialState = {
	id: 0,
	firstName: '',
	lastName: '',
	email: '',
	telephoneNumbers: [],
	country: '',
	birthday: '',
	password: '',
	phone: '',
	codeConfirmed: false,
	role: 'user',
	registrationStatus: 'password_protected',
	token: '',
	code: ''
};

export const accountReducer = (state = initialState, action = {}) => {
	let user;
	if(action.payload && action.payload.user) {
		user = action.payload.user;
	}
	switch (action.type) {
	case AccountAction.SET_COUNTRY:
		return {
			...state,
			country: action.payload.name
		};
	case AccountAction.LOGIN:
		return {
			...state,
			sessionToken: action.payload.sessionToken,
		};
	case AccountAction.SUCCESSFULLY_SIGN_UP:
		return {
			...state,
			id: user.id,
			email: user.email,
			token: action.payload.token
		};
	case AccountAction.USER_INFORMATION_SUCCESSFULLY_ADDED:
		return {
			...state,
			firstName: user.firstName,
			lastName: user.lastName,
			birthday: user.birthday,
			token: action.payload.token
		};
	case AccountAction.RECEIVED_CONFIRMATION_CODE:
		return {
			...state,
			phone: action.payload.phone,
			code: action.payload.code,
			token: action.payload.token
		};
	case AccountAction.CONFIRMATION_CODE_CHECKED:
		return {
			...state,
			codeConfirmed: true,
			token: action.payload.token
		};
	}
	return state;
};