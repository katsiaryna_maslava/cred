import {signup, updateUser, getConfirmationCode, checkConfirmationCode} from '../../../api';

const NAME = 'Account.';
export const AccountAction = {
    LOGIN: NAME + 'LOGIN',
    LOGOUT: NAME + 'LOGOUT',
    SUCCESSFULLY_SIGN_UP: NAME + 'SUCCESSFULLY_SIGN_UP',
    USER_INFORMATION_SUCCESSFULLY_ADDED: NAME + 'USER_INFORMATION_SUCCESSFULLY_ADDED',
    RECEIVED_CONFIRMATION_CODE: NAME + 'RECEIVED_CONFIRMATION_CODE',
    CONFIRMATION_CODE_CHECKED: NAME + 'CONFIRMATION_CODE_CHECKED',
    SET_COUNTRY: NAME + 'SET_COUNTRY',
};

export const createAccount = (user) => async (dispatch) => {
	try {
		user.role = 'user';
		user.registrationStatus = 'password_protected';
		const {data} = await signup(user);
		console.log('--------server----------', data);
		dispatch({type: AccountAction.SUCCESSFULLY_SIGN_UP, payload: {user: {...user, id: data.users[0].id}, token: data.token}});
	} catch (e) {
		console.log(e.response);
	}
};

export const updateUserInformation = (updateInfo, userId, token) => async (dispatch) => {
	try {
		const {data} = await updateUser(userId, updateInfo, token);
		console.log(data);
		dispatch({type: AccountAction.USER_INFORMATION_SUCCESSFULLY_ADDED, payload: {user: {...updateInfo}, token: data.token}});
	} catch (e) {
		console.log(e.response);
	}
};

export const getCode = (userId, phone, token) => async (dispatch) => {
	try {
		const {data} = await getConfirmationCode(userId, phone, token);
		console.log(data);
		dispatch({type: AccountAction.RECEIVED_CONFIRMATION_CODE, payload: {phone, confirmationCode: data.code, token: data.token}});
	} catch (e) {
		console.log(e.response);
	}
};

export const checkCode = (userId, code, token) => async (dispatch) => {
	try {
		const {data} = await checkConfirmationCode(userId, code, token);
		console.log(data);
		dispatch({type: AccountAction.CONFIRMATION_CODE_CHECKED, payload: {token: data.token}});
	} catch (e) {
		console.log(e.response);
	}
};