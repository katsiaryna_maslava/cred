import { createAccount, updateUserInformation, getCode, checkCode } from './actions';
import { compose } from 'recompose';
import { connect, Dispatch } from 'react-redux';

const enhancer = compose(
    connect(
        (state) => ({
            account: state.account,
        }),
        (dispatch) => ({
            accountActions: {
                createAccount: (user) => dispatch(createAccount(user)),
                updateUserInformation: (updateInfo, userId, token) => dispatch(updateUserInformation(updateInfo, userId, token)),
                getCode: (userId, phone, token) => dispatch(getCode(userId, phone, token)),
                checkCode: (userId, code, token) => dispatch(checkCode(userId, code, token)),
            }
        })
    )
);

export default enhancer;