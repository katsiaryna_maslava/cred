import { 
  takeEvery,
  takeLatest 
} from 'redux-saga/effects';
// import { 
//   setEmployer, 
//   getEmployer, 
//   createEmployer, 
//   updateEmployer, 
//   updateEmployerField, 
// } from './employer/saga';
// import { 
//   getUsers, 
// } from './users/saga';
import { 
  setUser, 
  getUser, 
  createUser, 
  deleteUser, 
  // updateEmployer, 
  updateUserField, 
} from './user/saga';
// import { EmployerActions } from './employer/actions';
// import { UsersActions } from './users/actions';
import { UserActions } from './user/actions';

function* sagas() {
    // yield takeEvery(UsersActions.GET, getUsers);
    yield takeEvery(UserActions.SET, setUser);
    // yield takeLatest(UserActions.GET, getUser);
    // yield takeLatest(UserActions.CREATE, createUser);
    // yield takeLatest(UserActions.DELETE, deleteUser);
    // yield takeLatest(UserActions.UPDATE, updateEmployer);
    yield takeLatest(UserActions.UPDATE_FIELD, updateUserField);
    // yield takeEvery(EmployerActions.SET, setEmployer);
    // yield takeLatest(EmployerActions.GET, getEmployer);
    // yield takeLatest(EmployerActions.CREATE, createEmployer);
    // yield takeLatest(EmployerActions.UPDATE, updateEmployer);
    // yield takeLatest(EmployerActions.UPDATE_FIELD, updateEmployerField);
    // yield takeEvery(EmployerActions.FIELD_UPDATED, updateEmployer);
}

export default sagas;