import { createUser, deleteUser, updateUserField, getUser, clearUser } from './actions';
import { compose } from 'recompose';
import { connect, Dispatch } from 'react-redux';

export const UserRegistrationStatus = {
    EMAIL_SENT: 'email_sent',
    PASSWORD_PROTECTED: 'password_protected',
    EMAIL_CONFIRMED: 'email_confirmed',
    NAME_SUBMITTED: 'name_submitted',
    GOAL_CREATED: 'goal_created',
    BANK_LINKED: 'bank_linked',
    USER_CREATED: 'user_created',
    PENDING_BANK_UPDATE: 'pending_bank_update',
};

const enhancer = compose(
    connect(
        (state) => ({
            user: state.user,
        }),
        (dispatch) => ({
            userActions: {
                getUser: (id) => dispatch(getUser(id)),
                createUser: () => dispatch(createUser()),
                deleteUser: (id) => dispatch(deleteUser(id)),
                clearUser: () => dispatch(clearUser()),
                updateUserField: (fieldName, value, sync) => 
                    dispatch(updateUserField(fieldName, value, sync)),
            }
        })
    )
);

export default enhancer;