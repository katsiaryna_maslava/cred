import { ActionInterface } from '../index';
import { UserActions } from './actions';

export const initialState = {
    errors: {},
    data: {},
    loading: false,
};

const reducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case UserActions.LOADING:
      return {...state, ...{loading: true}};
    case UserActions.SET:
      return {...state, ...{data: payload}, ...{loading: false}};
    case UserActions.ERROR:
      return {...state, ...{errors: payload}, ...{loading: false}};
    case UserActions.CLEAR:
      return initialState;
    default:
      return state;
  }
};

export default reducer;