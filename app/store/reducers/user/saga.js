import { 
  call, 
  put, 
  select, 
  cancelled,
} from 'redux-saga/effects';
import { delay } from 'redux-saga';
import {
  getUser as getUserApi,
  // updateEmployer as updateEmployerApi,
  createUser as createUserApi,
  deleteUser as deleteUserApi,
} from '../../api';
import validate from './validate';
import { UserActions } from './actions';
// import { UsersActions } from '../users/actions';

export function* setUser(action) {
  
  const { user } = yield select();
  try {
    // yield put({type: 'Errors.CLEAR'});
    
    const error = validate(user.data);
    
    yield put({type: UserActions.ERROR, payload: error});
    
  } catch (e) {
    yield put({type: UserActions.ERROR, payload: e.message});
  }
}

// export function* getUser(action) {
//   try {
//     yield put({type: UserActions.LOADING});
//     const { data } = yield call(getUserApi, action.payload.id);
//     yield put({type: UserActions.SET, payload: data});
//   } catch (e) {
//     yield put({type: UserActions.ERROR, payload: e.message});
//   }
// }

// export function* createUser(action) {

//   yield put({type: UserActions.LOADING });

//   const { user } = yield select();

//   if (Object.keys(user.errors).length !== 0) {
//     return;
//   }

//   if (Object.keys(user.data).length === 0) {
//     return;
//   }
  
//   try {

//     const { data, error } = yield call(createUserApi, user.data);
//     console.log('-4--4--4-4-4-4----', data, error);
    
//     yield put({type: UserActions.CLEAR });
//     // yield put({type: UsersActions.GET });
//     // action.payload.value(data.employer.id);

//   } catch (error) {
//     console.log('-5555555555----', error);

//     yield put({type: UserActions.ERROR, payload: { common: error.message}});
    
//   }
  
// }

// export function* deleteUser(action) {
  
//   try {
//     // yield put({type: UsersActions.LOADING });
//     const { data, error } = yield call(deleteUserApi, action.payload);
//     console.log('-delete----', data, error);
//     // yield put({type: UsersActions.GET });

//   } catch (error) {
//     console.log('-delete_error----', error);

//     yield put({type: UserActions.ERROR, payload: { common: error.message}});
    
//   }
  
// }

// export function* updateEmployer(action: any) {
  
//   const { employer, errors } = yield select();

//   if (Object.keys(errors.employer).length !== 0) {
//     return;
//   }
  
//   try {

//     const { data } = yield call(updateEmployerApi, employer);
//     yield put({type: EmployerActions.SET, payload: data});

//   } catch (e) {

//     yield put({type: EmployerActions.ERROR, message: e.message});
    
//   }

// }

export function* updateUserField(action) {
  
  const { user } = yield select();
  user.data[action.payload.fieldName] = action.payload.value;
  
  try {
    console.log('---------------------------4-------------------------');
    
    yield put({type: UserActions.SET, payload: user.data});
    yield call(delay, 500);

  } finally {
    
    // if (!(yield cancelled()) && action.payload.sync) {      
    //   yield put({type: UserActions.UPDATE});
    // }

  }
}