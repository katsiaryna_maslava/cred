// import { emailValidator } from '../../../constants';

const validate = ({
  firstName,
  lastName,
  contactFirstName,
  contactLastName,
  email,
  ein,
  zip,
  dayOfWeek,
}) => {

  let error = {};

  if (!firstName) {
    error.firstName = 'Should not be empty.';
  }

  if (!lastName) {
    error.lastName = 'Should not be empty.';
  }

  // if (!contactFirstName) {
  //   error.contactFirstName = 'Should not be empty.';
  // }

  // if (!contactLastName) {
  //   error.contactLastName = 'Should not be empty.';
  // }

  if (!email) {
    error.email = 'Should not be empty.';
  }

  // if (email && !emailValidator.test(email.toLowerCase())) {
  //   error.email = 'Not valid email.';
  // }

  // if (ein && ein.trim().length !== 0 && ein.length !== 9) {
  //   error.ein = 'Ein should consist of 9 number.';
  // }

  // if (ein && ein.trim().length !== 0 && !(/^[0-9]+$/.test(ein))) {
  //   error.ein = 'Ein should consist only from numbers.';
  // }

  // if (zip && zip.trim().length !== 0 && zip.length > 5) {
  //   error.zip = 'Zip should not be greater then 5 number.';
  // }

  // if (zip && zip.trim().length !== 0 && !(/^[0-9]+$/.test(zip))) {
  //   error.zip = 'Zip should consist only from numbers.';
  // }

  // if (dayOfWeek && dayOfWeek.length !== 0 && !(/^[0-9]+$/.test(dayOfWeek))) {
  //   error.dayOfWeek = 'Payroll Day of Week should be from 1 to 7.';
  // }

  // if (dayOfWeek && dayOfWeek.length !== 0 && parseInt(dayOfWeek, 10) === 0) {
  //   error.dayOfWeek = 'Payroll Day of Week should be from 1 to 7.';
  // }

  // if (dayOfWeek && dayOfWeek.length !== 0 && parseInt(dayOfWeek, 10) > 7) {
  //   error.dayOfWeek = 'Payroll Day of Week should be from 1 to 7.';
  // }

  return error;

};

export default validate;