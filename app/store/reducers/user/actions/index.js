import { Dispatch } from 'react-redux';

const NAME = 'User.';
export const UserActions = {
  SET: NAME + 'SET',
  GET: NAME + 'GET',
  CLEAR: NAME + 'CLEAR',
  CREATE: NAME + 'CREATE',
  UPDATE: NAME + 'UPDATE',
  DELETE: NAME + 'DELETE',
  UPDATE_FIELD: NAME + 'UPDATE_FIELD',
  LOADING: NAME + 'LOADING',
  ERROR: NAME + 'ERROR',
};

export const getUser = (id) => async (dispatch, getStore) => {

  dispatch({type: UserActions.GET, payload: {id}});

};

export const setUser = (user) => async (dispatch, getStore) => {

  dispatch({type: UserActions.SET, payload: user});

};

export const clearUser = () => async (dispatch, getStore) => {
  
  dispatch({type: UserActions.CLEAR});

};

export const createUser = () => async (dispatch, getStore) => {

  dispatch({type: UserActions.CREATE});

};

export const updateUser = () => async (dispatch, getStore) => {

  dispatch({type: UserActions.UPDATE});

};

export const deleteUser = (id) => async (dispatch, getStore) => {

  dispatch({type: UserActions.DELETE, payload: id});

};

export const updateUserField = (fieldName, value, sync = false) => 
async (dispatch, getStore) => {
  
  dispatch({type: UserActions.UPDATE_FIELD, payload: {fieldName, value, sync}});

};
