import {combineReducers} from 'redux';
import {accountReducer} from './account/reducer';
import user from './user/reducer';

export default combineReducers({
	account: accountReducer,
	user,
});