import {
	applyMiddleware,
	createStore,
} from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import sagas from './reducers/sagas';

const saga = createSagaMiddleware();

const middleware = [ thunk, saga ];

export const store = createStore(reducers, applyMiddleware(...middleware));

store.subscribe(() =>
  console.log('getState', store.getState())
);

saga.run(sagas);