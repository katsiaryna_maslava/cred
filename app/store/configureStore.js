import {
	applyMiddleware,
	createStore,
} from 'redux';
import ReduxThunk from 'redux-thunk';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducers from './reducers/index';
import {composeWithDevTools} from 'redux-devtools-extension';

const persistConfig = {
	key: 'root',
	storage,
	blacklist: ['transient'],
};

export const store = createStore(persistReducer(persistConfig, reducers), composeWithDevTools(applyMiddleware(ReduxThunk)));
