import React from 'react';
import {WebView} from 'react-native';

export default class WebviewScreen extends React.Component {
	render() {
		const { params } = this.props.navigation.state;
		const uri = params ? params.uri : null;

		return (
			<WebView source={{uri}}/>
		);
	}
}