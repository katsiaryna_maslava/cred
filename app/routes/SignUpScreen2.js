import React from 'react';
import PropTypes from 'prop-types';
import {
	View,
	Text,
	TextInput,
	ScrollView,
	StyleSheet,
	KeyboardAvoidingView,
	CheckBox,
	TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createAccount } from '../store/actions/index';
import { CustomButton, InputWrapper } from '../components';

import AddUserInformation from "./AddUserInformation";
import WebviewScreen from './WebviewScreen';

const buttonStyles = {
	nextBtn: [
		{backgroundColor: '#f4f3f3'}
	],
	nextBtnText: [
		{color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
	],
};

class SignUpScreen extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			email: '',
			password: '',
			termsChecked: true
		};
	}

	static propTypes = {
		navigation: PropTypes.object.isRequired,
		createAccount: PropTypes.func.isRequired
	};

	componentWillReceiveProps = (nextProps) => {
		if(nextProps.user && nextProps.user.id) {
			this.props.navigation.navigate('AddUserInformation');
		}
	}

	_inputHandler = (text, property) => {
		this.setState({ [property]: text });
	}

	_onSignUp = () => {
		this.props.createAccount({
			email: this.state.email,
			password: this.state.password,
			role: 'user',
			registrationStatus: 'password_protected'
		});
	}

	_checkBoxToggle = () => {
		this.setState({ termsChecked: !this.state.termsChecked });
	}

	_openTerms = () => {
		this.props.navigation.navigate('WebviewScreen',  {
			uri: 'https://google.com',
			title: 'Privacy Policy',
		});
	}

	render() {
		return (
			<KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
				<Text style={styles.header}>CRED</Text>
				<View style={styles.inputSectionsWrapper}>
					<InputWrapper
						handler={text => this._inputHandler(text, 'email')}
						label="Email (Username)"
						autoFocus={true}
						autoCapitalize='none'
					/>
					<InputWrapper
						handler={text => this._inputHandler(text, 'password')}
						label="Password"
						password={true}
						autoCapitalize='none'
					/>
					<View style={styles.wrapper}>
						<View style={styles.checkboxWrapper} >
							<CheckBox value={this.state.termsChecked} style={styles.checkbox} onChange={this._checkBoxToggle}/>
							<Text>I have read and agree to the </Text>
							<TouchableOpacity onPress={this._openTerms}>
								<Text style={styles.blueText} >terms</Text>
							</TouchableOpacity>
						</View>
						<View style={styles.secondTextRow}>
							<TouchableOpacity onPress={this._openTerms}>
								<Text style={styles.blueText} >& conditions</Text>
							</TouchableOpacity>
							<Text> and </Text>
							<TouchableOpacity onPress={this._openTerms}>
								<Text style={styles.blueText}>privacy notice.</Text>
							</TouchableOpacity>
						</View>
					</View>
				</View>
				<CustomButton
					style={buttonStyles.nextBtn}
					labelStyle={buttonStyles.nextBtnText}
					text='Next'
					onPress={this._onSignUp}
				/>
			</KeyboardAvoidingView>
		);
	}
}

const styles = StyleSheet.create({
	header: {
		flex: 1,
		fontSize: 20,
		fontWeight: 'bold',
		color: '#16d9a7',
		paddingTop: 40,
		paddingBottom: 0,
		textAlign: 'center'
	},
	inputSectionsWrapper: {
		flex: 4,
		paddingLeft: 40,
		paddingRight: 40
	},
	blueText: {
		color: '#718faa'
	},
	checkboxWrapper: {
		flexDirection: 'row',
		marginHorizontal: 20,
		alignItems: 'center'
	},
	checkbox: {
		marginLeft: -25
	},
	wrapper: {
		flex: 1,
		justifyContent: 'flex-start'
	},
	secondTextRow: {
		flexDirection: 'row',
		marginLeft: 25
	}
});


const mapDispatchToProps = (dispatch) => ({
	createAccount: bindActionCreators(createAccount, dispatch),
});

const mapStateToProps = (state) => ({
	user: state.account
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpScreen);