import React from 'react';
import { Image } from 'react-native';
import { StackNavigator, TabNavigator, TabBarBottom } from 'react-navigation';
import IntroScreen from './IntroScreen';

// import NewUserProfile from './Registration/NewUserProfile';

import UserProfileCredentials from './Registration/UserProfileCredentials';
import UserProfileName from './Registration/UserProfileName';
import UserProfileBirthday from './Registration/UserProfileBirthday';
import UserPhone from './Registration/UserPhone';
// import AddUserPhone from './AddUserPhone';
// import NewUserConfirmationCode from './NewUserConfirmationCode';
// import WebviewScreen from './WebviewScreen';
// import AddUserAddress from './Registration/AddUserAddress';
// import AddUserAddress2 from './Registration/AddUserAddress2';
// import UserLogin from "./UserLogin";
import UserAddress from './Registration/UserAddress';
import UserAddress2 from './Registration/UserAddress2';
import images from 'app/config/images';

import images from '../config/images';

import ChooseCryptoCurrency from './BuyCrypto/ChooseCryptoCurrency';
import CryptoLanding from './BuyCrypto/CryptoLanding';

const BuyCryptoTab = StackNavigator(
  {
    ChooseCryptoCurrency: {
      screen: ChooseCryptoCurrency
    },
    CryptoLanding: {
      screen: CryptoLanding
    }
  },
  {
    headerMode: 'none'
  }
);

const TabsNavigator = TabNavigator(
  {
    Portfolio: {
      screen: BuyCryptoTab,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: 'PORTFOLIO'
      })
    },
    Prices: {
      screen: BuyCryptoTab,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: 'PRICES'
      })
    },
    News: {
      screen: BuyCryptoTab,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: 'NEWS'
      })
    },
    Crypto: {
      screen: BuyCryptoTab,
      navigationOptions: ({ navigation }) => ({
        tabBarLabel: 'CRYPTO'
      })
    }
  },
  {
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: 'rgb(38, 84, 124)',
      inactiveTintColor: 'rgb(102, 103, 102)',
      allowFontScaling: false,
      activeBackgroundColor: 'rgb(240, 240, 240)',
      inactiveBackgroundColor: 'rgb(240, 240, 240)',
      labelStyle: {
        fontFamily: 'avenir_roman',
        fontSize: 11,
        fontWeight: 'bold'
      }
    },
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let image;
        if (routeName === 'Portfolio') {
          image = images.PORTFOLIO_TAB_ICON;
        } else if (routeName === 'Prices') {
          image = images.PRICE_TAB_ICON;
        } else if (routeName === 'News') {
          image = images.NEWS_TAB_ICON;
        } else if (routeName === 'Crypto') {
          image = images.CRYPTO_TAB_ICON;
        }
        return <Image source={image} />;
      }
    })
  }
);

export const RootNavigator = StackNavigator(
  {
    Intro: {
      screen: IntroScreen
    },
    // NewUserProfile: {
    //   screen: NewUserProfile
    // },
    UserProfileCredentials: {
      screen: UserProfileCredentials
    },
    UserProfileName: {
      screen: UserProfileName
    },
    UserProfileBirthday: {
      screen: UserProfileBirthday
    },
    UserPhone: {
      screen: UserPhone
    },
      UserAddress: {
          screen: UserAddress,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: 'CRYPTO'
          })
      },
      UserAddress2: {
          screen: UserAddress2,
          navigationOptions: ({ navigation }) => ({
              tabBarLabel: 'CRYPTO'
          })
      }
    },
    // AddUserPhone: {
    //   screen: AddUserPhone
    // },
    // NewUserConfirmationCode: {
    //   screen: NewUserConfirmationCode
    // },
    // WebviewScreen: {
    //   screen: WebviewScreen
    // },
    // TabsNavigator: {
    //   screen: TabsNavigator
    // },
    // AddUserAddress: {
    //   screen: AddUserAddress,
    // },
    // AddUserAddress2: {
    //     screen: AddUserAddress2,
    // },
    // UserLogin: {
    //   screen: UserLogin,
    // },
  {
      initialRouteName: 'UserAddress',
    navigationOptions: () => ({
      headerStyle: { height: 50, /*backdropColor:'white', */ elevation: 0 },
      headerTitleStyle: {
        fontFamily: 'avenir_roman',
        fontSize: 20,
        fontWeight: 'bold',
        flex: 1,
        alignItems: 'center',
        marginTop: 2
      }
    })
  }
);
