import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomButton, InputWrapper } from '../components';
import { bindActionCreators } from 'redux';
// import {getCode} from '../store/actions';
// import { accountActions } from '../store/actions/index';

const buttonStyles = {
    nextBtn: [
        {backgroundColor: '#f4f3f3'}
    ],
    nextBtnText: [
        {color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ],
};

class AddUserPhone extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            header: null,
            headerLeft: null,
            headerRight: null,
        };
    };

    constructor(props) {
        super(props);

        this.state = {
            phoneNumber: null,
        };
    }

    static propTypes = {
        navigation: PropTypes.object.isRequired,
        // accountActions: PropTypes.object.isRequired
    };

	componentWillReceiveProps = (nextProps) => {
		if(nextProps.user && nextProps.user.phone) {
			this.props.navigation.navigate('NewUserConfirmationCode');
		}
	}

    _inputHandler = (text, property) => {
        this.setState({ [property]: text });
    };

    _onNextBtn = () => {
        const {id, token} = this.props.user;
        this.props.getCode({...this.props.user, phone: this.state.phoneNumber});
        this.props.navigation.navigate('NewUserConfirmationCode');
    };

    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
                <Text style={styles.header}>CRED</Text>
				<View style={styles.inputSectionsWrapper}>
					<InputWrapper
						handler={text => this._inputHandler(text, 'phoneNumber')}
						label="Phone"
						autoFocus={true}
						autoCapitalize='none'
						error={this.props.errors.phone}
						maxLength={15}
					/>
                </View>
                <CustomButton
                    style={buttonStyles.nextBtn}
                    labelStyle={buttonStyles.nextBtnText}
					disabledLabelStyle={buttonStyles.disabledNextButtonText}
                    text='Next'
                    onPress={this._onNextBtn}
                />
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#16d9a7',
        paddingTop: 40,
        paddingBottom: 0,
        textAlign: 'center'
    },
    inputSectionsWrapper: {
        flex: 4,
        paddingLeft: 40,
        paddingRight: 40
    },
    blueText: {
        color: '#718faa',
    },
    checkboxWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 20
    }
});


const mapDispatchToProps = (dispatch) => ({
	getCode: bindActionCreators(getCode, dispatch),
});

const mapStateToProps = (state) => ({
	user: state.account.data,
	errors: state.account.errors
});

export default connect(mapStateToProps, mapDispatchToProps)(AddUserPhone);
