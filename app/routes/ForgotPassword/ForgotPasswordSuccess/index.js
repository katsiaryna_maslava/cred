import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, Modal, ScrollView } from 'react-native';
import { Ionicons, EvilIcons } from '@expo/vector-icons';
import images from 'app/config/images';
import Header from 'app/components/Header';
import styles from './styles';

class ForgotPasswordSuccess extends React.Component {
  static navigationOptions = () => ({
    header: props => (
      <Header
        {...props}
      />
    )
  });

  state = {
  };

  render() {
    return (
      <View style={styles.page} behavior="padding" enabled>
        <TouchableOpacity style={styles.getButton}>
          <Ionicons name="ios-arrow-forward" style={styles.getButtonIcon} />
        </TouchableOpacity>
      </View>
    );
  }
}

export default ForgotPasswordSuccess;
