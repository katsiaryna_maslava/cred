import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    StyleSheet,
    KeyboardAvoidingView,
    TouchableOpacity,
    Alert,
	ToastAndroid
} from 'react-native';
import { connect } from 'react-redux';
import CustomButton from '../components/CustomButton';
import { bindActionCreators } from 'redux';
import { login } from '../store/actions/index';
import InputWrapper from '../components/InputWrapper';
import validateEmail from '../utils/validateEmail';
import validatePassword from '../utils/validatePassword';


const buttonStyles = {
    nextBtn: [
        {backgroundColor: '#f4f3f3'}
    ],
    nextBtnText: [
        {color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ],
};

class UserLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
			errors: {
				emailError: false,
				passwordError: false
			}
        };
    }

    static propTypes = {
        navigation: PropTypes.object.isRequired,
        login: PropTypes.func.isRequired
    };

    _inputHandler = (text, property) => {
        this.setState({ [property]: text });
    };

    _onLogin = () => {
		this.setState({
			errors: {
				emailError: !validateEmail(this.state.email),
				passwordError: !validatePassword(this.state.password)
			}
		}, () => {
			const {emailError, passwordError} = this.state.errors;
			if(!emailError && !passwordError) {
				this.props.login({
					email: this.state.email,
					password: this.state.password,
				});
			}
		});
    };

     optionalConfigObject = {
        title: "Authentication Required",
        color: "#e00606",
        failbackLabel: "Show Passcode",
    };

    _onTouchIdHandler = () => {
		this.setState({ popupShowed: true });
		// Finger.isSensorAvailable()
		// 	.then((isAvailable) => {
		// 		ToastAndroid.show('Sensor is available and is waiting for touch', ToastAndroid.SHORT);
		// 		this.touchAuth()
		// 	})
		// 	.catch(error => {
		// 		ToastAndroid.show(error, ToastAndroid.SHORT);
		// 	});
        // TouchID.authenticate('to demo this react-native component', this.optionalConfigObject)
        //     .then(success => {
        //         Alert.alert('Authenticated Successfully');
        //     })
        //     .catch(error => {
        //         console.log(error);
        //         Alert.alert('Authentication Failed');
        //     });
    };

	handleFingerprintDismissed = () => {
		this.setState({ popupShowed: false });
	};

    render() {
		const { errorMessage, popupShowed } = this.state;
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
                <Text style={styles.header}>CRED</Text>
                <View style={styles.inputSectionsWrapper}>
                    <InputWrapper
                        handler={text => this._inputHandler(text, 'email')}
                        label='Email'
                        autoFocus={true}
                        autoCapitalize='none'
						error={this.state.errors.emailError && 'Invalid email format.'}
                    />
                    <InputWrapper
                        handler={text => this._inputHandler(text, 'password')}
                        label='Password'
                        password={true}
                        autoCapitalize='none'
						error={this.state.errors.passwordError && 'Must be at least 6 characters with at least 1 number and 1 capital letter.'}
                    />
                </View>
				{popupShowed && (
					<FingerprintPopup
						style={styles.popup}
						handlePopupDismissed={this.handleFingerprintDismissed}
					/>
				)}
                <View>
                    <TouchableOpacity onPress={this._onTouchIdHandler}>
                        <Text style={styles.touchID} on>Login with Touch ID</Text>
                    </TouchableOpacity>
                </View>
                <CustomButton
                    style={buttonStyles.nextBtn}
                    labelStyle={buttonStyles.nextBtnText}
                    text='Login'
                    onPress={this._onLogin}
                />
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#16d9a7',
        paddingTop: 40,
        paddingBottom: 0,
        textAlign: 'center'
    },
    inputSectionsWrapper: {
        flex: 4,
        paddingLeft: 40,
        paddingRight: 40
    },
    touchID: {
        color: '#718faa',
        fontWeight: 'bold',
        textAlign: 'center',
    }
});


const mapDispatchToProps = (dispatch) => ({
    login: bindActionCreators(login, dispatch),
});

const mapStateToProps = (state) => ({
	user: state.account.data,
	errors: state.account.errors
});

export default connect(mapStateToProps, mapDispatchToProps)(UserLogin);