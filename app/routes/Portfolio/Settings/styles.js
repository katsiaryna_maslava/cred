import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  wrapper: {
    flex: 1,
		paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
