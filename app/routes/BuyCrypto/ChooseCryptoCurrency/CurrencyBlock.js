/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Octicons, Ionicons } from '@expo/vector-icons';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { currencyBlockStyles as styles } from './styles';

type Props = {
  currencyIcon: React.node,
  title: string,
  onInfoIconPress: Function,
  trendDirection: string,
  percentage: string,
  backgroundImage: any,
  onPress: Function
};

class CurrencyBlock extends React.Component<Props, {}> {
  static propTypes = {
    currencyIcon: PropTypes.node,
    title: PropTypes.string,
    onInfoIconPress: PropTypes.func,
    trendDirection: PropTypes.string,
    percentage: PropTypes.string,
    backgroundImage: PropTypes.any,
    onPress: PropTypes.func
  };

  static defaultProps = {
    onInfoIconPress: () => {}
  };

  render() {
    const props = this.props;
    return (
      <TouchableOpacity style={styles.wrapper} onPress={props.onPress}>
        <View style={styles.imageWrapper}>
          <Image
            source={props.backgroundImage}
            resizeMode="cover"
            style={styles.image}
          />
        </View>
        <View style={styles.titleWrapper}>
          <View style={styles.nameWrapper}>
            <Text style={styles.name}>{props.title}</Text>
          </View>
          <View style={styles.trendWrapper}>
            <Octicons
              style={[
                styles.directionIcon,
                props.trendDirection === 'up' && styles.directionIconUp,
                props.trendDirection === 'down' && styles.directionIconDown
              ]}
              name={`triangle-${props.trendDirection}`}
            />
            <Text style={styles.percentage}>{props.percentage}</Text>
            <TouchableOpacity onPress={props.onInfoIconPress}>
              <Ionicons
                name="ios-information-circle-outline"
                style={styles.infoIcon}
              />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default CurrencyBlock;
