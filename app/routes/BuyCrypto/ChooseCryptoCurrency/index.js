import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, Modal, ScrollView } from 'react-native';
import { Ionicons, EvilIcons } from '@expo/vector-icons';
import CurrencyBlock from './CurrencyBlock';
import images from '../../../config/images';
import Header from '../../../components/Header';
import styles from './styles';

class ChooseCryptoCurrency extends React.Component {
  static navigationOptions = () => ({
    header: props => (
      <Header
        {...props}
        title="Choose Currency"
        rightIcon={<Ionicons name="ios-mail-outline" style={styles.mailIcon} />}
        leftIcon={<EvilIcons name="gear" style={styles.mailIcon} />}
      />
    )
  });

  state = {
    modalVisible: false,
    modalTitle: ''
  };

  _openModal = title => e => {
    this.setState({ modalVisible: true, modalTitle: title });
  };

  _closeModal = () => {
    this.setState({ modalVisible: false, modalTitle: '' });
  };

  _goToCryptoLanding = (type, image) => event => {
    this.props.navigation.navigate('CryptoLanding', { type, image });
  };

  _renderModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={this._closeModal}>
        <View style={styles.infoModalWrapper}>
          <View style={styles.infoModalContent}>
            <View style={styles.closeIconWrapper}>
              <TouchableOpacity onPress={this._closeModal}>
                <Ionicons name="md-close" style={styles.modalCloseIcon} />
              </TouchableOpacity>
            </View>
            <Text style={styles.modalTitle}>{this.state.modalTitle}</Text>
            <ScrollView>
              <Text style={styles.modalCurrencyContent}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book.{'\n\n'}
                It has survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
                {'\n\n'}
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum.
              </Text>
            </ScrollView>
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return (
      <View style={styles.page} behavior="padding" enabled>
        <ScrollView>
          <CurrencyBlock
            currencyIcon={null}
            title="Bitcoin"
            onInfoIconPress={this._openModal('Bitcoin')}
            trendDirection="up"
            percentage="+20%"
            backgroundImage={images.GRADIENT_1}
            onPress={this._goToCryptoLanding('Bitcoin', images.GRADIENT_1)}
          />
          <CurrencyBlock
            currencyIcon={null}
            title="BitcoinCash"
            onInfoIconPress={this._openModal('BitcoinCash')}
            trendDirection="up"
            percentage="+10%"
            backgroundImage={images.GRADIENT_2}
            onPress={this._goToCryptoLanding('BitcoinCash', images.GRADIENT_2)}
          />
          <CurrencyBlock
            currencyIcon={null}
            title="Ethereum"
            onInfoIconPress={this._openModal('Ethereum')}
            trendDirection="up"
            percentage="+30%"
            backgroundImage={images.GRADIENT_3}
            onPress={this._goToCryptoLanding('Ethereum', images.GRADIENT_3)}
          />
          <CurrencyBlock
            currencyIcon={null}
            title="Litcoin"
            onInfoIconPress={this._openModal('Litcoin')}
            trendDirection="down"
            percentage="-20%"
            backgroundImage={images.GRADIENT_4}
            onPress={this._goToCryptoLanding('Litcoin', images.GRADIENT_4)}
          />
          <TouchableOpacity style={styles.getButton}>
            <Text style={styles.getButtonText}>Get $10</Text>
            <Ionicons name="ios-arrow-forward" style={styles.getButtonIcon} />
          </TouchableOpacity>
        </ScrollView>

        {this._renderModal()}
      </View>
    );
  }
}

export default ChooseCryptoCurrency;
