import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  page: {
    flex: 1
  },
  mailIcon: {
    color: 'rgb(38, 84, 124)',
    fontSize: 30
  },
  getButton: {
    margin: 10,
    padding: 15,
    backgroundColor: 'rgb(6, 214, 160)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  getButtonText: {
    fontSize: 20,
    color: 'white',
    fontFamily: 'avenir_roman',
    flex: 1,
    textAlign: 'center'
  },
  getButtonIcon: {
    color: 'white',
    fontSize: 20,
  },
  infoModalWrapper: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    paddingVertical: 35,
    paddingHorizontal: 15,
    flex: 1,
    justifyContent: 'center'
  },
  infoModalContent: {
    borderRadius: 10,
    backgroundColor: 'rgba(255,255,255,0.9)',
    paddingVertical: 15,
    paddingHorizontal: 35,
    marginVertical: 35
  },
  closeIconWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  modalCloseIcon: {
    fontSize: 30,
    color: '#999'
  },
  modalTitle: {
    fontFamily: 'avenir_roman',
    fontSize: 25,
    color: 'rgb(102, 103, 102)',
    marginBottom: 10
  },
  modalCurrencyContent: {
    fontFamily: 'avenir_roman',
    fontSize: 18,
    color: 'rgb(102, 103, 102)',
    lineHeight: 25
  }
});

export const currencyBlockStyles = StyleSheet.create({
  wrapper: {
    minHeight: 115,
    flex: 1
  },
  imageWrapper: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    flex: 1
  },
  image: {
    flex: 1,
    width: null,
    height: null
  },
  titleWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  nameWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 10
  },
  name: {
    fontSize: 25,
    color: 'white',
    fontFamily: 'avenir_roman'
  },
  trendWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 15,
    paddingHorizontal: 10
  },
  directionIcon: {
    fontSize: 25
  },
  directionIconUp: {
    color: 'rgb(6, 214, 160)'
  },
  directionIconDown: {
    color: 'rgb(239, 71, 111)'
  },
  percentage: {
    fontSize: 25,
    color: 'white',
    fontFamily: 'avenir_roman'
  },
  infoIcon: {
    color: 'white',
    fontSize: 25,
    marginLeft: 15,
    marginTop: 0
  }
});
