/* @flow */

import React from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import { FontAwesome, Octicons } from '@expo/vector-icons';
import styles from './styles';

class CryptoLanding extends React.Component {
  scrollViewRef: any;

  static navigationOptions = () => ({
    header: null,
    headerTransparent: true
  });

  state = {
    isScrolling: false,
    showBackButton: true,
    scrollStartOffset: 0
  };

  _goBack = () => {
    this.props.navigation.goBack();
  };

  _onScrollBeginDrag = e => {
    this.setState({
      isScrolling: true,
      scrollStartOffset: e.nativeEvent.contentOffset.y
    });
  };

  _onScrollEndDrag = e => {
    const { scrollStartOffset } = this.state;
    const endOffset = e.nativeEvent.contentOffset.y;
    this.setState({ isScrolling: false });
    if (scrollStartOffset - endOffset > 0) {
      this._pullDown();
    } else {
      this._scrollUp();
    }
  };

  _pullDown = () => {
    this.scrollViewRef.scrollTo({ x: 0, y: 0, animated: true });
    this.setState({ showBackButton: false });
  };

  _scrollUp = () => {
    this.scrollViewRef.scrollToEnd({ animated: true });
    this.setState({ showBackButton: true });
  };

  _renderSectionOne() {
    const { isScrolling } = this.state;
    const { type, image } = this.props.navigation.state.params;
    return (
      <View
        style={[styles.pageSectionContent, isScrolling && { opacity: 0.6 }]}>
        <View style={styles.section1UpperView}>
          <View>
            <View style={styles.section1CurrencyTitleWrapper}>
              <Text style={styles.section1CurrencyTitle}>{type}</Text>
            </View>
            <Text style={styles.section1CurrencyValue}>.00006</Text>
            <Text style={styles.section1CurrencyShort}>BTC</Text>
          </View>
          <View style={styles.periodSelectionWrapper}>
            <View style={styles.periodSelectionItem}>
              <Text style={styles.periodSelectionItemText}>1 DAY</Text>
            </View>
            <View
              style={[
                styles.periodSelectionItem,
                styles.selectedPeriodSelectionItem
              ]}>
              <Text style={styles.periodSelectionItemText}>1 MONTH</Text>
            </View>
            <View style={styles.periodSelectionItem}>
              <Text style={styles.periodSelectionItemText}>ALL-DAY</Text>
            </View>
          </View>
        </View>
        {!isScrolling && (
          <TouchableOpacity style={styles.scrollUp} onPress={this._scrollUp}>
            <FontAwesome name="angle-double-up" style={styles.scrollUpIcon} />
            <Text style={styles.scrollUpText}>Scroll Up</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }

  _renderSectionTwo() {
    const { isScrolling } = this.state;
    const { type, image } = this.props.navigation.state.params;
    return (
      <View
        style={[styles.pageSectionContent, isScrolling && { opacity: 0.6 }]}>
        <View style={styles.upperView}>
          <View style={styles.imageWrapper}>
            <Image
              source={image}
              style={styles.image}
              resizeMode="cover"
            />
          </View>
          {!isScrolling && (
            <TouchableOpacity style={styles.pullDown} onPress={this._pullDown}>
              <FontAwesome
                name="angle-double-down"
                style={styles.pullDownIcon}
              />
              <Text style={styles.pullDownText}>Pull Down</Text>
            </TouchableOpacity>
          )}
          <View style={styles.titleWrapper}>
            <View style={styles.nameWrapper}>
              <Text style={styles.name}>{type}</Text>
            </View>
            <View style={styles.trendWrapper}>
              <Octicons style={styles.directionIcon} name={`triangle-up`} />
              <Text style={styles.percentage}>+25%</Text>
            </View>
          </View>
        </View>
        <View style={styles.bottomView}>
          <Text style={styles.bottomViewTitle}>
            Select a Way to Invest in Bitcoin
          </Text>
          <View style={styles.investOption}>
            <View style={styles.optionItem}>
              <Text style={styles.optionItemText}>Setup a Round Up</Text>
              <FontAwesome name="angle-right" style={styles.optionIcon} />
            </View>
            <View style={styles.optionItem}>
              <Text style={styles.optionItemText}>
                Setup a Recurring Investment
              </Text>
              <FontAwesome name="angle-right" style={styles.optionIcon} />
            </View>
            <View style={[styles.optionItem, { borderBottomWidth: 0 }]}>
              <Text style={styles.optionItemText}>One-Time Investment</Text>
              <FontAwesome name="angle-right" style={styles.optionIcon} />
            </View>
          </View>
          <Text style={styles.disclaimerText}>
            Disclaimer: Each token has its own investment settings, independent
            from other tokens on the Cred platform.
          </Text>
        </View>
      </View>
    );
  }

  render() {
    const { showBackButton } = this.state;
    return (
      <View style={styles.page}>
        <ScrollView
          ref={ref => (this.scrollViewRef = ref)}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          onLayout={e => {
            this.scrollViewRef.scrollToEnd({ animated: false });
          }}
          onScrollBeginDrag={this._onScrollBeginDrag}
          onScrollEndDrag={this._onScrollEndDrag}>
          {this._renderSectionOne()}
          {this._renderSectionTwo()}
        </ScrollView>
        {showBackButton && (
          <View style={styles.backButtonWrapper}>
            <TouchableOpacity style={styles.backButton} onPress={this._goBack}>
              <FontAwesome name="angle-left" style={styles.backButtonIcon} />
              <Text style={styles.backButtonText}>Back</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }
}

export default CryptoLanding;
