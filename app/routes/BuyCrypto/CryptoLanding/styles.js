import { StyleSheet, Dimensions } from 'react-native';
let { height } = Dimensions.get('window');
export default StyleSheet.create({
  page: {
    flex: 1,
    paddingTop: 24,
    backgroundColor: '#FFFFFF'
  },
  pageSectionContent: {
    height: height - 70
  },
  upperView: {
    flex: 1
  },
  backButtonWrapper: {
    flexDirection: 'row',
    position: 'absolute',
    top: 25,
    left: 5
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10
  },
  backButtonIcon: {
    fontSize: 30,
    color: 'white'
  },
  backButtonText: {
    fontSize: 20,
    color: 'white',
    fontFamily: 'avenir_roman',
    marginLeft: 10
  },
  section1UpperView: {
    flex: 1,
    paddingTop: 60,
    paddingBottom: 30,
    backgroundColor: '#FFD21C',
    justifyContent: 'space-between'
  },
  section1CurrencyTitleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  section1CurrencyTitle: {
    fontSize: 35,
    fontFamily: 'avenir_roman',
    color: 'white'
  },
  section1CurrencyValue: {
    fontSize: 50,
    fontFamily: 'avenir_roman',
    color: 'white',
    textAlign: 'center',
    marginTop: 20
  },
  section1CurrencyShort: {
    fontSize: 20,
    fontFamily: 'avenir_roman',
    color: 'white',
    textAlign: 'center',
    marginTop: 0
  },
  periodSelectionWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 2,
    borderRadius: 50,
    borderColor: '#F3BC00',
    marginHorizontal: 15
  },
  periodSelectionItem: {
    padding: 15,
    flex: 1,
    borderRadius: 50
  },
  selectedPeriodSelectionItem: {
    backgroundColor: '#F3BC00'
  },
  periodSelectionItemText: {
    fontSize: 16,
    fontFamily: 'avenir_roman',
    color: 'white',
    textAlign: 'center'
  },
  scrollUp: {
    backgroundColor: 'white',
    paddingVertical: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  scrollUpIcon: {
    color: '#125181',
    fontSize: 20
  },
  scrollUpText: {
    marginLeft: 5,
    color: '#125181',
    fontSize: 15,
    fontFamily: 'avenir_roman'
  },
  imageWrapper: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    flex: 1
  },
  image: {
    flex: 1,
    width: null,
    height: null
  },
  pullDown: {
    marginTop: 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  pullDownIcon: {
    color: 'white',
    fontSize: 15
  },
  pullDownText: {
    marginLeft: 5,
    color: 'white',
    fontSize: 15,
    fontFamily: 'avenir_roman'
  },
  titleWrapper: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20
  },
  nameWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15
  },
  name: {
    fontSize: 25,
    color: 'white',
    fontFamily: 'avenir_roman'
  },
  trendWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15
  },
  directionIcon: {
    fontSize: 25,
    color: 'rgb(6, 214, 160)'
  },
  percentage: {
    fontSize: 25,
    color: 'white',
    fontFamily: 'avenir_roman'
  },
  bottomView: {
    flex: 1
  },
  bottomViewTitle: {
    fontSize: 20,
    paddingVertical: 20,
    fontFamily: 'avenir_roman',
    textAlign: 'center'
  },
  disclaimerText: {
    fontSize: 15,
    padding: 20,
    marginBottom: 10,
    color: 'rgb(179, 179, 179)',
    fontFamily: 'avenir_roman',
    textAlign: 'center',
    lineHeight: 19
  },
  investOption: {
    paddingLeft: 15,
    backgroundColor: '#FCFCFC',
    borderTopWidth: 2,
    borderTopColor: '#e5e5e5',
    borderBottomWidth: 2,
    borderBottomColor: '#e5e5e5'
  },
  optionItem: {
    padding: 15,
    paddingVertical: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#e5e5e5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  optionItemText: {
    fontFamily: 'avenir_roman',
    fontSize: 15,
    color: 'rgb(102, 103, 102)'
  },
  optionIcon: {
    fontSize: 25,
    color: 'rgb(102, 103, 102)'
  }
});
