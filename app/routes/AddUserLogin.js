import React, { Component } from 'react';
import {
    Text,
    ScrollView,
    StyleSheet
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import { accountActions } from '../store/actions/index';
import { CustomButton, InputWrapper } from '../components';

export class AddUserLogin extends Component {
    constructor(props) {
        super(props);
        state = {
            inputAddress: '',
        }
    }
    render() {
        return (
            <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
                <Text style={styles.header}>CRED</Text>
                <View style={styles.inputSectionsWrapper}>
                    <InputWrapper handler={text => this._inputHandler(text, 'email')} label="Email (Username)"/>
                    <InputWrapper handler={text => this._inputHandler(text, 'password')} label="Password" password={true}/>
                    <View style={styles.checkboxWrapper} >
                        <CheckBox value={this.state.termsChecked} onChange={this._checkBoxToggle}/>
                        <Text>I have read and agree to the <Text style={styles.blueText}>terms & conditions </Text>and <Text style={styles.blueText}>privacy notice.</Text></Text>
                    </View>
                </View>
                <CustomButton
                    style={buttonStyles.nextBtn}
                    labelStyle={buttonStyles.nextBtnText}
                    text='Next'
                    onPress={this._onSignUp}
                />
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#16d9a7',
        paddingTop: 40,
        paddingBottom: 0,
        textAlign: 'center'
    },
    inputSectionsWrapper: {
        flex: 4,
        paddingLeft: 40,
        paddingRight: 40
    },
});

const mapDispatchToProps = (dispatch) => ({
    // accountActions: bindActionCreators(accountActions, dispatch),
});
