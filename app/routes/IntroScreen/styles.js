import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	root: {
		flex: 1,
		backgroundColor: '#fff',
	},
	header: {
		flex: 1,
		fontSize: 20,
		fontWeight: 'bold',
		color: '#16d9a7',
		paddingTop: 40,
		paddingBottom: 10,
		textAlign: 'center'
	},
	introBlocksWrapper: {
		flex: 9,
		paddingLeft: 40,
		paddingRight: 40
	}
});