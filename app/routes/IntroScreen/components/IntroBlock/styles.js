import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	blockWrapper: {
		borderBottomColor: '#f2f2f2',
		borderBottomWidth: 1,
		paddingTop: 20,
		paddingBottom: 20,
		flex: 1,
		flexDirection: 'row'
	},
	textsWrapper: {
		width: '57%'
	},
	imageWrapper: {
		width: '43%'
	},
	title: {
		fontSize: 18,
		color: '#24557e'
	},
	description: {
		color: '#24557e'
	}
});