import React from 'react';
import {View, Text, Image} from 'react-native';
import styles from './styles';

const IntroBlock = ({title, description, imageSource}) => (
    <View style={styles.blockWrapper}>
        <View style={styles.textsWrapper}>
            <Text style={styles.title}>{title.toUpperCase()}</Text>
            <Text style={styles.description}>{description}</Text>
        </View>
        <View style={styles.imageWrapper}>
            <Image source={imageSource}/>
        </View>
    </View>
)

export default IntroBlock;