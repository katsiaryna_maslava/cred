import React from 'react';
import styled from 'styled-components/native';
import {View, Text} from 'react-native';
import IntroBlock from './components/IntroBlock';
import CustomButton from '../../components/CustomButton';
import styles from './styles';

const LoginButtonHolder = styled.View`
	flex: 1;
`;

const buttonStyles = {
	loginBtn: [
		{borderRightColor: '#f9f9f9', borderRightWidth: 5}
	],
	loginBtnText: [
		{color: '#24557e'}
	],
	signUpBtn: [

	],
	signUpBtnText: [
		{color: '#16d9a7'}
	],
	getBtn: [
		{backgroundColor: '#4cdbb7'}
	],
	getBtnText: [{color: '#fff'}]
};

const IntroScreen = ({onSignUp, onLogin}) => (
	<View style={styles.root}>
		<Text style={styles.header}>CRED</Text>
		<View style={styles.introBlocksWrapper}>
			<IntroBlock
				title="Round up spare change"
				description="Automatically save without thinking about it."
				imageSource={require('../../../assets/images/teaserRoundUpIcon.png')}
			/>
			<IntroBlock
				title="Invest in crypto currencies"
				description="Micro-investing in crypto is safe and responsible."
				imageSource={require('../../../assets/images/teaserSaveIcon.png')}
				reverse={true}
			/>
			<IntroBlock
				title="Track your performance"
				description="Buy and sell ... track of it all in one place."
				imageSource={require('../../../assets/images/teaserAssistanceIcon.png')}
			/>
		</View>
		<CustomButton
			style={buttonStyles.getBtn}
			labelStyle={buttonStyles.getBtnText}
			text='Get $ 10'
		/>
		<View style={[{flex:1, flexDirection:'row'}]}>
			<LoginButtonHolder>
				<CustomButton
					style={buttonStyles.loginBtn}
					labelStyle={buttonStyles.loginBtnText}
					text='Login'
					onPress={onLogin}
				/>
			</LoginButtonHolder>
			<LoginButtonHolder>
				<CustomButton
					style={buttonStyles.signUpBtn}
					labelStyle={buttonStyles.signUpBtnText}
					text='Sign Up'
					onPress={onSignUp}
				/>
			</LoginButtonHolder>
		</View>
	</View>
)

export default IntroScreen;