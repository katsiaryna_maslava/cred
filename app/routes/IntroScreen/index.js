import {compose, withHandlers} from 'recompose';
import View from "./view";

const enhancer = compose(
  withHandlers({
    onSignUp: ({navigation}) => () => {
        navigation.navigate('UserProfileCredentials')
    },
    onLogin: ({navigation}) => () => {
        // navigation.navigate('LoginScreen')
    },
  })
);

export default enhancer(View);