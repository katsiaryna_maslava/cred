import React, { Component } from 'react';

import {
    Text,
    View,
    StyleSheet,
    Alert,
	Image,
    KeyboardAvoidingView,
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import { checkCode } from '../store/actions/index';
import CodeInput from 'react-native-confirmation-code-input';
import CustomButton from '../components/CustomButton';

class NewUserConfirmationCode extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            confirmationCode: null,

        }
    }

	componentWillReceiveProps = (nextProps) => {
		if(nextProps.user && nextProps.user.codeConfirmed) {
			Alert.alert(
				'Confirmation Code',
				'Successful!',
				[{text: 'OK'}],
				{ cancelable: false }
			);
		}
	}

    _onFinishCheckingCode = (code) => {
        this.props.checkCode({...this.props.user, code});
    }

    _onNextBtn = () => {
        this.props.navigation.navigate('AddUserAddress')
    }

   render() {
       return (
           <KeyboardAvoidingView style={styles.inputWrapper} behavior="padding" enabled>
			   <Text style={styles.header}>CRED</Text>
			   <View  style={styles.imageStyle}>
				   <Image source={require('../../assets/images/confirmationCodeIcon.png')}/>
               </View>
               <Text style={styles.inputLabel}>Confirmation Code</Text>
               <CodeInput
				   keyboardType='numeric'
                   activeColor='rgb(179, 179, 179)'
                   inactiveColor='rgb(179, 179, 179)'
                   autoFocus={false}
                   ignoreCase={true}
                   inputPosition='center'
                   size={50}
                   onFulfill={(isValid) => this._onFinishCheckingCode(isValid)}
                   containerStyle={{ marginTop: 30, backgroundColor: '#fff', paddingBottom: 0, marginBottom: 0, height: 10 }}
                   codeInputStyle={{ borderWidth: 1.5 }}
               />
               <Text style={styles.text}>Check your messages for your code.</Text>
			   <CustomButton
				   style={buttonStyles.nextBtn}
				   labelStyle={buttonStyles.nextBtnText}
                   disabledLabelStyle={buttonStyles.disabledNextButtonText}
				   text='Next'
				   onPress={this._onNextBtn}
				   enabled={this.props.user.codeConfirmed}
			   />
           </KeyboardAvoidingView>
       );
   }
}

const buttonStyles = {
	nextBtn: [

    ],
	nextBtnText: [
        {color: 'rgb(26, 185, 127)', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ],
    disabledNextButton: [

    ],
	disabledNextButtonText: [
        {color: 'rgb(179, 179, 179)', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ]
};

const styles = StyleSheet.create({
    inputWrapper: {
        paddingHorizontal: 40,
        backgroundColor: '#fff',
        flex: 1,
    },
    inputLabel: {
        color: 'rgb(102, 103, 102)',
        fontSize: 25,
        textAlign: 'left',
        marginTop: 10
    },
	imageStyle: {
	   alignItems: 'center'
    },
	text: {
		color: 'rgb(102, 103, 102)',
		fontSize: 15,
        flex: 1
    }
});

const mapDispatchToProps = (dispatch) => ({
	checkCode: bindActionCreators(checkCode, dispatch),
});

const mapStateToProps = (state) => ({
	user: state.account.data,
	errors: state.account.errors
});

export default connect(mapStateToProps, mapDispatchToProps)(NewUserConfirmationCode);