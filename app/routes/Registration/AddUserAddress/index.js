import PropTypes from 'prop-types';
import React from 'react';
import {
    BackHandler,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import {NavigationActions} from "react-navigation";

class Header extends React.PureComponent {
    static propTypes = {
        title: PropTypes.string,
        titleIcon: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.func]),
        renderLeftIcon: PropTypes.func,
    };
    static defaultProps = {
        title: 'HEADER',
        titleIcon: null,
    };

    renderLeftIcon() {
        if (this.props.renderLeftIcon) {
            return this.props.renderLeftIcon();
        }
        return null;
    }

    renderIcon() {
        const {titleIcon, iconStyle} = this.props;
        if (titleIcon) {
            if (typeof(titleIcon) === 'string') {
                return <Text style={iconStyle}>{titleIcon}</Text>;
            }
            if (typeof(titleIcon) === 'function') {
                return titleIcon(this.props);
            }
            return titleIcon;
        }
        return null;
    }

    render() {
        const {title, titleStyle} = this.props;
        return (
            <View style={headerStyles.headerContainer}>
                {this.renderIcon()}
                <Text style={titleStyle}>
                    {title}
                </Text>
                <View style={headerStyles.leftIconWrapper}>
                    {this.renderLeftIcon()}
                </View>
            </View>
        );
    }
}

const headerStyles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 70,
        alignItems: 'center',
    },
    leftIconWrapper: {
        position: 'absolute',
        left: 0,
    },
});

class AddressRow extends React.PureComponent {
    static propTypes = {
        row: PropTypes.object,
    };
    static defaultProps = {
        row: {},
    };

    render() {
        const {row} = this.props;
        return (
            <View style={rowStyles.rowContainer}>
                <Text>{row.description}</Text>
                <Ionicons name={'ios-arrow-forward'} size={24} style={rowStyles.arrow}/>
            </View>
        );
    }
}

const rowStyles = StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    arrow: {
        flex: 1,
        textAlign: 'right',
    },
});

export default class AddUserAddress extends React.Component {
    onBack = () => {
        const backAction = NavigationActions.back();
        this.props.navigation.dispatch(backAction);
    };

    renderArrow = () => {
        return (
            <TouchableOpacity onPress={this.onBack}>
                <View style={styles.arrowBackContainer}>
                    <Ionicons name='ios-arrow-back' size={40}/>
                    <Text style={styles.backLabel}>
                        Back
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };

    onSelectAddress = (data, details = null) => {
        this.props.navigate(details);

        const navigateAction = NavigationActions.navigate({
            routeName: AddUserAddress2,
            params: {
                addressDetails: details,
            }
        });
        this.props.navigation.dispatch(navigateAction);
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBack);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBack);
    }

    renderInput() {
        return (
            <View style={styles.inputContainer}>
                <View style={styles.labelContainer}>
                    <Ionicons name={'ios-contact'} size={22} style={styles.labelIcon}/>
                    <Text style={styles.label}>
                        Home Address
                    </Text>
                </View>
                <GooglePlacesAutocomplete
                    style={styles.autoComplete}
                    placeholder=''
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'search'}
                    listViewDisplayed='auto'
                    fetchDetails={true}
                    renderRow={row => <AddressRow row={row}/>}
                    onPress={this.onSelectAddress}
                    query={{
                        key: GOOGLE_API_KEY,
                        language: 'en',
                        types: '',
                    }}
                    styles={{
                        container: styles.completeContainer,
                        textInputContainer: styles.textInputContainer,
                        textInput: styles.textInput,
                    }}
                    enablePoweredByContainer={false}
                    nearbyPlacesAPI='GooglePlacesSearch'
                    GooglePlacesSearchQuery={{
                        rankby: 'distance',
                        types: 'food',
                    }}
                    filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                    debounce={200}
                />
            </View>
        );
    }

    render() {
        return (
            <View style={styles.flex}>
                <Header
                    title={'Cred'}
                    titleIcon={'\u{2699}'}
                    titleStyle={styles.title}
                    iconStyle={styles.title}
                    renderLeftIcon={this.renderArrow}
                />
                {this.renderInput()}
            </View>
        );
    }
}
