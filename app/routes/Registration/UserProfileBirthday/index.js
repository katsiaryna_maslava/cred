import {compose, withHandlers, withState} from 'recompose';
import View from "./view";

const enhancer = compose(
    withState('birthday', 'setBirthday'),
    withHandlers({
        onNext: ({navigation}) => () => {
            navigation.navigate('UserPhone')
        }
    })
);

export default enhancer(View);