import React from 'react';
import {
	View,
	Text,
	KeyboardAvoidingView,
	CheckBox,
	TouchableOpacity
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import CustomButton from '../../../components/CustomButton';
import InputWrapper from '../../../components/InputWrapper';
import styles from './styles';

const buttonStyles = {
    nextBtn: [
        {backgroundColor: '#f4f3f3'}
    ],
    nextBtnText: [
        {color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ],
};

const UserProfileBirthday = ({birthday, setBirthday, onNext}) => (
    <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
        <Text style={styles.header}>CRED</Text>
        <View style={styles.inputSectionsWrapper}>
            <Text style={styles.birthdayLabel}>Birthday</Text>
            <DatePicker
                style ={{flex: 3, width: '100%'}}
                date={birthday}
                format="YYYY-MM-DD"
                showIcon={false}
                customStyles={{
                    dateInput: {
                        borderBottomColor: '#6282a2',
                        borderWidth: 0,
                        borderBottomWidth: 1
                    },
                    dateText: {
                        fontSize: 20,
                        textAlign: 'left'
                    },
                    placeholderText: {
                        fontSize: 20,
                        textAlign: 'left'
                    }
                }}
                onDateChange={(date) => {setBirthday(date);}}/>
        </View>
        <CustomButton
            style={buttonStyles.nextBtn}
            labelStyle={buttonStyles.nextBtnText}
            text='Next'
            onPress={onNext}
        />
    </KeyboardAvoidingView>
) 

export default UserProfileBirthday;