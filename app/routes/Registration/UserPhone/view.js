import React from 'react';
import {
	View,
	Text,
	KeyboardAvoidingView,
	TouchableOpacity
} from 'react-native';
import CustomButton from '../../../components/CustomButton';
import InputWrapper from '../../../components/InputWrapper';
import styles from './styles';

const buttonStyles = {
    nextBtn: [
        {backgroundColor: '#f4f3f3'}
    ],
    nextBtnText: [
        {color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ],
};

const UserPhone = ({phoneNumber, setPhoneNumber, onNext}) => (
    <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
        <Text style={styles.header}>CRED</Text>
        <View style={styles.inputSectionsWrapper}>
            <InputWrapper
                handler={text => setPhoneNumber(text)}
                label="Phone"
                autoFocus={true}
                autoCapitalize='none'
                maxLength={15}
            />
        </View>
        <CustomButton
            style={buttonStyles.nextBtn}
            labelStyle={buttonStyles.nextBtnText}
            disabledLabelStyle={buttonStyles.disabledNextButtonText}
            text='Next'
            onPress={onNext}
        />
    </KeyboardAvoidingView>
) 

export default UserPhone;