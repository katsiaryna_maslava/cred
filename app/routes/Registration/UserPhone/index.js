import {compose, withHandlers, withState} from 'recompose';
import View from "./view";

const enhancer = compose(
    withState('phoneNumber', 'setPhoneNumber'),
    withHandlers({
        onNext: ({navigation}) => () => {
            navigation.navigate('Intro')
        }
    })
);

export default enhancer(View);
