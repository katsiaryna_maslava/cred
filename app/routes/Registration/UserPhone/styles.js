import { StyleSheet } from 'react-native';

export default StyleSheet.create({
	header: {
        flex: 1,
        fontSize: 20,
        fontWeight: 'bold',
        color: '#16d9a7',
        paddingTop: 40,
        paddingBottom: 0,
        textAlign: 'center'
    },
    inputSectionsWrapper: {
        flex: 4,
        paddingLeft: 40,
        paddingRight: 40
    },
    blueText: {
        color: '#718faa',
    },
    checkboxWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginHorizontal: 20
    }
});