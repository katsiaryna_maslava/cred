import React from 'react';
import {
    BackHandler,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import styles from './styles';

const UserAddress2 = ({navigation, notSupported=false, onButtonPress}) => {
    const marker = navigation.state.params.addressDetails;
    const loc = marker.geometry.location;
    return (
        <View style={styles.flex}>
            <View style={styles.flex}>
                <View style={styles.mapFooter}>
                    <Text style={styles.addressText}>
                        {marker.formatted_address}
                    </Text>
                    <Text style={styles.addintionalText}>
                        Add Address Line 2
                    </Text>
                </View>

                <View style={styles.flex}>
                    <MapView
                        style={styles.flex}
                        initialRegion={{
                            latitude: loc.lat,
                            longitude: loc.lng,
                            latitudeDelta: 1.0922,
                            longitudeDelta: 1.0421,
                        }}
                    >
                        <Marker
                            coordinate={{
                                latitude: loc.lat,
                                longitude: loc.lng,
                            }}
                            title={'test'}
                            description={'test'}
                        />
                    </MapView>
                    {notSupported && (
                        <View style={styles.supportMessage}>
                            <Text style={styles.mainSupportText}>
                                Your state is not supported at this time.
                            </Text>
                            <Text style={styles.secondarySupportText}>
                                We will email you as soon as Cred is available in your state.
                            </Text>
                        </View>)}
                </View>

                <View style={styles.buttonWrapper}>
                    <TouchableOpacity onPress={onButtonPress}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>
                                {notSupported ? 'Okay, got it.' : 'Looks correct!'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default UserAddress2;
