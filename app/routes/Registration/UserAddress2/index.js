import {connect} from 'react-redux';
import {compose, withHandlers, withState} from 'recompose';
import {bindActionCreators} from 'redux';
import {sendAddress} from '../../../store/actions/account';
import View from "./view";

const enhancer = compose(
    withState(),
    withHandlers({
        onButtonPress: ({navigation, sendAddress, user}) => () => {
            const marker = navigation.state.params.addressDetails;
            sendAddress(user, marker.something);
        }
    })
);

const mapDispatchToProps = (dispatch) => ({

});

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(enhancer(View))
