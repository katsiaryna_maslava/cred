import {compose, withHandlers, withState} from 'recompose';
import withAccount from '../../../store/reducers/account';
import View from "./view";

const enhancer = compose(
    withAccount,
    withState('email', 'setEmail'),
    withState('password', 'setPassword'),
    withHandlers({
        onNext: ({email, password, accountActions, navigation}) => () => {
            console.log('-----onNext', email, password);
            accountActions.createAccount({
                email,
                password
            });
            navigation.navigate('UserProfileName')
        },
        onLogin: ({navigation}) => () => {
            // navigation.navigate('LoginScreen')
        },
    })
);

export default enhancer(View);