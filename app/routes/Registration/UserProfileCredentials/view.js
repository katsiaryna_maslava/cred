import React from 'react';
import {
	View,
	Text,
	KeyboardAvoidingView,
	CheckBox,
	TouchableOpacity
} from 'react-native';
import CustomButton from '../../../components/CustomButton';
import InputWrapper from '../../../components/InputWrapper';
import styles from './styles';

const buttonStyles = {
	nextBtn: [
		{backgroundColor: '#f4f3f3'}
	],
	nextBtnText: [
		{color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
	],
};

const UserProfileCredentials = ({email, setEmail, password, setPassword, onNext}) => (
    <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
        <Text style={styles.header}>CRED</Text>
        <View style={styles.inputSectionsWrapper}>
            <InputWrapper
                handler={text => setEmail(text)}
                label="Email (Username)"
                autoFocus={true}
                autoCapitalize='none'
            />
            <InputWrapper
                handler={text => setPassword(text)}
                label="Password"
                password={true}
                autoCapitalize='none'
            />
            {/* <View style={styles.wrapper}>
                <View style={styles.checkboxWrapper} >
                    <CheckBox value={this.state.termsChecked} style={styles.checkbox} onChange={this._checkBoxToggle}/>
                    <Text>I have read and agree to the </Text>
                    <TouchableOpacity onPress={this._openTerms}>
                        <Text style={styles.blueText} >terms</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.secondTextRow}>
                    <TouchableOpacity onPress={this._openTerms}>
                        <Text style={styles.blueText} >& conditions</Text>
                    </TouchableOpacity>
                    <Text> and </Text>
                    <TouchableOpacity onPress={this._openTerms}>
                        <Text style={styles.blueText}>privacy notice.</Text>
                    </TouchableOpacity>
                </View>
            </View> */}
        </View>
        <CustomButton
            style={buttonStyles.nextBtn}
            labelStyle={buttonStyles.nextBtnText}
            text='Next'
            onPress={onNext}
        />
    </KeyboardAvoidingView>
) 

export default UserProfileCredentials;