import React from 'react';
import {
	View,
	Text,
	KeyboardAvoidingView,
	CheckBox,
	TouchableOpacity
} from 'react-native';
import CustomButton from '../../../components/CustomButton';
import InputWrapper from '../../../components/InputWrapper';
import styles from './styles';

const buttonStyles = {
    nextBtn: [
        {backgroundColor: '#f4f3f3'}
    ],
    nextBtnText: [
        {color: '#20d9a8', textAlign: 'right', width: '100%', paddingRight: 30, fontSize: 18}
    ],
};

const UserProfileName = ({firstName, setFirstName, lastName, setLastName, onNext}) => (
    <KeyboardAvoidingView style={{flex:1}} behavior="padding" enabled>
        <Text style={styles.header}>CRED</Text>
        <View style={styles.inputSectionsWrapper}>
            <InputWrapper handler={text => setFirstName(text)} label="First Name"/>
            <InputWrapper handler={text => setLastName(text)} label="Last Name"/>
        </View>
        <CustomButton
            style={buttonStyles.nextBtn}
            labelStyle={buttonStyles.nextBtnText}
            text='Next'
            onPress={onNext}
        />
    </KeyboardAvoidingView>
) 

export default UserProfileName;