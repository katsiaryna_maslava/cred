import {compose, withHandlers, withState} from 'recompose';
import View from "./view";

const enhancer = compose(
    withState('firstName', 'setFirstName'),
    withState('lastName', 'setLastName'),
    withHandlers({
        onNext: ({navigation}) => () => {
            navigation.navigate('UserProfileBirthday')
        }
    })
);

export default enhancer(View);