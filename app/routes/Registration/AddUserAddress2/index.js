import PropTypes from 'prop-types';
import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    BackHandler,
    View,
} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import {createAccount} from "../../../store/actions";

class Header extends React.PureComponent {
    static propTypes = {
        title: PropTypes.string,
        titleIcon: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.func]),
        renderLeftIcon: PropTypes.func,
    };
    static defaultProps = {
        title: 'HEADER',
        titleIcon: null,
    };
    renderLeftIcon() {
        if (this.props.renderLeftIcon) {
            return this.props.renderLeftIcon();
        }
        return null;
    }

    renderIcon() {
        const {titleIcon, iconStyle} = this.props;
        if (titleIcon) {
            if (typeof(titleIcon) === 'string') {
                return <Text style={iconStyle}>{titleIcon}</Text>;
            }
            if (typeof(titleIcon) === 'function') {
                return titleIcon(this.props);
            }
            return titleIcon;
        }
        return null;
    }

    render() {
        const {title, titleStyle} = this.props;
        return (
            <View style={headerStyles.headerContainer}>
                {this.renderIcon()}
                <Text style={titleStyle}>
                    {title}
                </Text>
                <View style={headerStyles.leftIconWrapper}>
                    {this.renderLeftIcon()}
                </View>
            </View>
        );
    }
}

const headerStyles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: '100%',
        height: 70,
        alignItems: 'center',
    },
    leftIconWrapper: {
        position: 'absolute',
        left: 0,
    }
});

class AddressRow extends React.PureComponent {
    static propTypes = {
        row: PropTypes.object,
    };
    static defaultProps = {
        row: {},
    };
    render() {
        const {row} = this.props;
        return (
            <View style={rowStyles.rowContainer}>
                <Text>{row.description}</Text>
                <Ionicons name={'ios-arrow-forward'} size={24} style={rowStyles.arrow}/>
            </View>
        );
    }
}

const rowStyles = StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    arrow: {
        flex: 1,
        textAlign: 'right',
    },
});

class AddUserAddress2 extends React.Component {
    state = {
        notSupported: false,
    };

    onBack = () => {
        return true;
    };

    renderSupportMessage() {
        if (this.state.notSupported) {
            return (
                <View style={styles.supportMessage}>
                    <Text style={styles.mainSupportText}>
                        Your state is not supported at this time.
                    </Text>
                    <Text style={styles.secondarySupportText}>
                        We will email you as soon as Cred is available in your state.
                    </Text>
                </View>
            );
        }
    }

    renderMap() {
        const marker = this.props.navigation.state.params.addressDetails;
        const loc = marker.geometry.location;
        return (
            <View style={styles.flex}>
                <MapView
                    style={styles.flex}
                    initialRegion={{
                        latitude: loc.lat,
                        longitude: loc.lng,
                        latitudeDelta: 1.0922,
                        longitudeDelta: 1.0421,
                    }}
                >
                    <Marker
                        coordinate={{
                            latitude: loc.lat,
                            longitude: loc.lng,
                        }}
                        title={'test'}
                        description={'test'}
                    />
                </MapView>
                {this.renderSupportMessage()}
            </View>
        );
    }


    onButtonPress = () => {
        //this.setState({notSupported: true});
        const marker = this.props.navigation.state.params.addressDetails;
        console.log(marker);
        this.props.sendAddress(user, marker.something, this._onSent)
    }

    _onSent = (data) => {
        console.log(data);
        this.setState({notSupported: true});

    }

    renderMapPage() {
        return (
            <View style={styles.flex}>
                <View style={styles.mapFooter}>
                    <Text style={styles.addressText}>
                        {marker.formatted_address}
                    </Text>
                    <Text style={styles.addintionalText}>
                        Add Address Line 2
                    </Text>
                </View>
                {this.renderMap()}
                <View style={styles.buttonWrapper}>
                    <TouchableOpacity onPress={this.onButtonPress}>
                        <View style={styles.button}>
                            <Text style={styles.buttonText}>
                                {this.state.notSupported ? 'Okay, got it.': 'Looks correct!'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

    renderArrow = () => {
        return (
            <TouchableOpacity onPress={this.onBack}>
                <View style={styles.arrowBackContainer}>
                    <Ionicons name='ios-arrow-back' size={40}/>
                    <Text style={styles.backLabel}>
                        Back
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBack);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.onBack);
    }
    render() {
        return (
            <View style={styles.flex}>
                <Header
                    title={'Cred'}
                    titleIcon={'\u{2699}'}
                    titleStyle={styles.title}
                    iconStyle={styles.title}
                    renderLeftIcon={this.renderArrow}
                />
                {this.renderMapPage()}
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    createAccount: bindActionCreators(sendAddress, dispatch),
});

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AddUserAddress2)