import {
    EvilIcons,
    Ionicons,
} from '@expo/vector-icons';
import PropTypes from 'prop-types';
import React from 'react';
import {
    Modal,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {GOOGLE_API_KEY} from '../../../config/constants';
import styles from './styles';

class AddressRow extends React.PureComponent {
    static propTypes = {
        row: PropTypes.object,
    };
    static defaultProps = {
        row: {},
    };

    render() {
        const {row} = this.props;
        return (
            <View style={rowStyles.rowContainer}>
                <Text>{row.description}</Text>
                <Ionicons name={'ios-arrow-forward'} size={24} style={rowStyles.arrow}/>
            </View>
        );
    }
}

const rowStyles = StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
    },
    arrow: {
        flex: 1,
        textAlign: 'right',
    },
});

const UserAddress = ({onSelectAddress}) => (
        <View style={styles.flex}>
            <View style={styles.inputContainer}>
                <View style={styles.labelContainer}>
                    <Ionicons name={'ios-contact'} size={22} style={styles.labelIcon}/>
                    <Text style={styles.label}>
                        Home Address
                    </Text>
                </View>
                <GooglePlacesAutocomplete
                    style={styles.autoComplete}
                    placeholder=''
                    minLength={2}
                    autoFocus={false}
                    returnKeyType={'search'}
                    listViewDisplayed='auto'
                    fetchDetails={true}
                    renderRow={row => <AddressRow row={row}/>}
                    onPress={onSelectAddress}
                    query={{
                        key: GOOGLE_API_KEY,
                        language: 'en',
                        types: '',
                    }}
                    styles={{
                        container: styles.completeContainer,
                        textInputContainer: styles.textInputContainer,
                        textInput: styles.textInput,
                    }}
                    enablePoweredByContainer={false}
                    nearbyPlacesAPI='GooglePlacesSearch'
                    GooglePlacesSearchQuery={{
                        rankby: 'distance',
                        types: 'food',
                    }}
                    filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']}
                    debounce={200}
                />
            </View>
        </View>
    );

export default UserAddress;
