import {NavigationActions} from 'react-navigation';
import {compose, withHandlers, withState} from 'recompose';
import View from "./view";

const enhancer = compose(
    withHandlers({
        onSelectAddress: ({navigation}) => (data, details = null) => {
            const navigateAction = NavigationActions.navigate({
                routeName: 'UserAddress2',
                params: {
                    addressDetails: details,
                }
            });
            navigation.dispatch(navigateAction);
        }
    })
);

export default enhancer(View);
