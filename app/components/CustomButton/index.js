import React from 'react';
import PropTypes from 'prop-types';
import {
	Text,
	View,
	TouchableOpacity
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

export default class CustomButton extends React.Component {

	static propTypes = {
		style: PropTypes.array,
		disabledStyle: PropTypes.array,
		labelStyle: PropTypes.array,
		iconStyle: PropTypes.array,
		disabledLabelStyle: PropTypes.array,
		text: PropTypes.string,
		enabled: PropTypes.bool,
		onPress: PropTypes.func,
		direction: PropTypes.oneOf(['forward', 'back']),
	};

	_backIcon = () => {
		if ( this.props.direction == 'back') {
			return <Ionicons name={`ios-arrow-back`} style={styles.iconLeftStyle} />
		}
	};
	
	_forwardIcon = () => {
		if ( this.props.direction == 'forward') {
			return <Ionicons name={`ios-arrow-forward`} style={styles.iconRightStyle} />
		}
	};
	
	render() {

		let enabled = this.props.hasOwnProperty('enabled') ? this.props.enabled : true;

		let btnStyle = styles.getButton;
		if ( this.props.style !== null ) {
			btnStyle = [
				...btnStyle,
				this.props.style
			];
		}

		let btnStyleDisabled = [...btnStyle];
		if ( this.props.disabledStyle !== null ) {
			btnStyleDisabled = [
				...btnStyleDisabled,
				this.props.disabledStyle
			];
		}

		let lblStyle = styles.defaultLabelStyle;
		if ( this.props.labelStyle !== null ) {
			lblStyle = [
				...lblStyle,
				this.props.labelStyle
			];
		}

		let lblStyleDisabled = [...lblStyle];//, '#0ffff0'];
		if ( this.props.disabledLabelStyle !== null ) {
			lblStyleDisabled = [
				...lblStyleDisabled,
				this.props.disabledLabelStyle
			];
		}
		
		if (enabled) {
			return(
				<TouchableOpacity onPress={this.props.onPress} style={[{flexDirection: 'row'},btnStyle]}>
					<View>
						{this._backIcon()}
						<Text style={lblStyle}>{this.props.text}</Text>
            {this._forwardIcon()}
					</View>
				</TouchableOpacity>
			);
		}
		else {
			return(
				<View style={[{flexDirection: 'row'},btnStyle]}>
					<Text style={lblStyleDisabled}>{this.props.text}</Text>
				</View>
			);
		}
	}
}
