import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  getButton: {
    margin: 15,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iconLeftStyle: {
		color: '#ffffff',
		fontSize: 30,
		marginLeft: 10,
		position: 'absolute',
		left: 0
	},
  iconRightStyle: {
		color: '#ffffff',
		fontSize: 30,
		marginRight: 10,
		position: 'absolute',
		right: 0
	},
  defaultLabelStyle: {
		margin : 20,
		// textColor:'#000000'
	}

});
