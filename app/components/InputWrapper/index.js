import React from 'react';
import {View, Text, Image, TextInput, StyleSheet} from 'react-native';

export default class InputWrapper extends React.Component {
	render() {
		const {label, password, handler, autoFocus, autoCapitalize, keyboardType, maxLength, error} = this.props;
		return (
			<View>
				<Text style={styles.label}>{label}</Text>
				<TextInput style={styles.input}
				   	placeholder=''
				   	placeholderTextColor={'#E1E1E1'}
				   	onChangeText={handler}
					secureTextEntry={password}
					underlineColorAndroid={!error ? '#6282a2' : 'red'}
					autoFocus={autoFocus}
					autoCapitalize={autoCapitalize}
				    keyboardType={keyboardType}
				    maxLength={maxLength}/>
				{error ? <Text style={styles.errorMessage}>{error}</Text> : null}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	label: {
		paddingTop: 20,
		fontSize: 15
	},
	input: {
		borderBottomColor: '#6282a2',
		paddingBottom: 18,
		fontSize: 20
	},
	errorMessage: {
		color: 'red',
		fontSize: 10
	}
});