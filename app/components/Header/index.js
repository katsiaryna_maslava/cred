/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import idx from 'idx';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

type Props = {
  scenes: Array<any>,
  rightIcon: React.node,
  leftIcon: React.node,
  backButtonStyle: any,
  wrapperStyle: any,
  leftIconWrapperStyle: any,
  titleWrapperStyle: any,
  rightIconWrapperStyle: any,
};

class Header extends React.Component<Props, {}> {
  static propTypes = {
    scenes: PropTypes.array,
    rightIcon: PropTypes.node,
    leftIcon: PropTypes.node,
    backButtonStyle: PropTypes.any,
    wrapperStyle: PropTypes.any,
    leftIconWrapperStyle: PropTypes.any,
    titleWrapperStyle: PropTypes.any,
    rightIconWrapperStyle: PropTypes.any,
  };

  _renderRightIcon() {
    const props = this.props;
    if (props.rightIcon) {
      return (
        <TouchableOpacity style={styles.rightIcon}>
          {props.rightIcon}
        </TouchableOpacity>
      );
    }
    return null;
  }

  _renderLeftIcon() {
    const props = this.props;
    if (props.leftIcon) {
      return (
        <TouchableOpacity style={styles.leftIcon}>
          {props.leftIcon}
        </TouchableOpacity>
      );
    } else if (idx(props, _ => _.scenes.length) > 0) {
      return (
        <TouchableOpacity
          style={styles.leftIcon}
          onPress={() => {
            props.navigation.goBack();
          }}>
          <Ionicons
            name="ios-arrow-back"
            style={[styles.backIcon, props.backButtonStyle]}
          />
          <Text style={[styles.backText, props.backButtonTextStyle]}>Back</Text>
        </TouchableOpacity>
      );
    }
    return null;
  }

  _renderTitle() {
    const props = this.props;
    if (!props.title) {
      return null;
    }
    return <Text style={styles.titleText}>{props.title}</Text>;
  }

  render() {
    const props = this.props;
    return (
      <View style={[styles.wrapper, props.wrapperStyle]}>
        <View style={[styles.leftIconWrapper, props.leftIconWrapperStyle]}>
          {this._renderLeftIcon()}
        </View>
        <View style={[styles.titleWrapper, props.titleWrapperStyle]}>
          {this._renderTitle()}
        </View>
        <View style={[styles.rightIconWrapper, props.rightIconWrapperStyle]}>
          {this._renderRightIcon()}
        </View>
      </View>
    );
  }
}

export default Header;
