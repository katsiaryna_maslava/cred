import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    ...Platform.select({
      ios: {
        paddingTop: 20,
        height: 70
      },
      android: {
        paddingTop: 20,
        height: 70
      }
    }),
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center'
  },
  leftIconWrapper: {
    minWidth: 50
  },
  leftIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10
  },
  backIcon: {
    fontSize: 30,
    color: 'rgb(38, 84, 124)'
  },
  backText: {
    fontSize: 16,
    color: 'rgb(38, 84, 124)',
    marginLeft: 8
  },
  titleWrapper: {
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  titleText: {
    fontFamily: 'avenir_roman',
    fontSize: 20,
    alignItems: 'center',
    marginTop: 2,
    color: 'rgb(89, 90, 92)'
  },
  rightIconWrapper: {
    width: 50,
    height: 50
  },
  rightIcon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
