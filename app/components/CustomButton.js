import React from 'react';
import PropTypes from 'prop-types';
import {
	Text,
	View,
	TouchableOpacity
} from 'react-native';
// import { Ionicons } from '@expo/vector-icons';

const defaultViewStyle = [
	{
		// backdropColor:'blue',
		justifyContent:'center',
		alignItems:'center'
	}
];

const defaultLabelStyle = [
	{
		margin : 20,
		// textColor:'#000000'
	}
];

export default class CustomButton extends React.Component {

	static propTypes = {
		style: PropTypes.array,
		disabledStyle: PropTypes.array,
		labelStyle: PropTypes.array,
		disabledLabelStyle: PropTypes.array,
		text: PropTypes.string,
		enabled: PropTypes.bool,
		onPress: PropTypes.func,
		direction: PropTypes.oneOf(['right', 'left'])
	};

	render() {

		let enabled = this.props.hasOwnProperty('enabled') ? this.props.enabled : true;

		let btnStyle = defaultViewStyle;
		if ( this.props.style !== null ) {
			btnStyle = [
				...btnStyle,
				this.props.style
			];
		}

		let btnStyleDisabled = [...btnStyle];
		if ( this.props.disabledStyle !== null ) {
			btnStyleDisabled = [
				...btnStyleDisabled,
				this.props.disabledStyle
			];
		}

		let lblStyle = defaultLabelStyle;
		if ( this.props.labelStyle !== null ) {
			lblStyle = [
				...lblStyle,
				this.props.labelStyle
			];
		}

		let lblStyleDisabled = [...lblStyle];//, '#0ffff0'];
		if ( this.props.disabledLabelStyle !== null ) {
			lblStyleDisabled = [
				...lblStyleDisabled,
				this.props.disabledLabelStyle
			];
		}

		if (enabled) {
			return(
				<TouchableOpacity onPress={this.props.onPress}>
					<View style={[{flexDirection: 'row'},btnStyle]}>
						<Text style={lblStyle}>{this.props.text}</Text>
                         {/*<Ionicons name={`chevron-${this.props.direction}`} size={32} color="green" />*/}
					</View>
				</TouchableOpacity>
			);
		}
		else {
			return(
				<View style={btnStyleDisabled}>
					<Text style={lblStyleDisabled}>{this.props.text}</Text>
				</View>
			);
		}
	}
}
