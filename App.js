/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { Provider } from 'react-redux';
import { Font } from 'expo';
import { store } from './app/store';
import { Platform, StatusBar, View } from 'react-native';
import { RootNavigator } from './app/routes/RootNavigator';

export default class App extends React.Component {
  state = {
    fontLoaded: false
  };

  async componentDidMount() {
    await Font.loadAsync({
      avenir_light: require('./assets/fonts/AvenirLTStd-Light.ttf'),
      avenir_roman: require('./assets/fonts/AvenirLTStd-Roman.ttf'),
      arial: require('./assets/fonts/arial.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  render() {
    if (!this.state.fontLoaded) {
      return null;
    }

    return (
      <Provider store={store}>
        <View style={{ flex: 1, backgroundColor: 'red' }}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          {Platform.OS === 'android' && <View />}
          <RootNavigator />
        </View>
      </Provider>
    );
  }
}
